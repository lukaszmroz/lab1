import reader.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class MainFrame extends CenteredFrame implements ActionListener {

    private ArrayList<JButton> buttons;

    public MainFrame() {
        super("Parser");
        setup();
    }

    private void setup() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Shutdown process on closing window

        // Layout
        JPanel panel = new JPanel();
        String[] buttonNames = new String[] {"Parsuj przez URL", "Parsuj z pliku"};
        buttons = new ArrayList<>();

        for (String buttonName : buttonNames) {
            JButton button = new JButton(buttonName);
            button.addActionListener(this);
            panel.add(button);
            buttons.add(button);
        }

        setMainPanel(panel, true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();
        AbstractReader reader;
        if (button.equals(buttons.get(0))) {
            reader = ReaderFactory.create(ReaderType.URL, PathGetter.getPath(ReaderType.URL));
        } else {
            reader = ReaderFactory.create(ReaderType.File, PathGetter.getPath(ReaderType.File));
        }

        if (reader == null) {
            return;
        }

        Extractor extracter = new Extractor(reader.read());
        List<String> urls = extracter.extractUrls();
        new ResultsFrame(urls);
    }
}