import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class ResultsFrame extends CenteredFrame implements ActionListener {

    private List<String> results;
    private JButton showListButton;
    private boolean isListShowing = false;
    private JScrollPane listScrollPane = new JScrollPane();
    private JPanel listPanel = new JPanel();
    private JPanel firstCardPanel;
    private JList resultList;

    public ResultsFrame(List<String> results) {
        super("Wyniki");
        this.results = results;
        setup();
    }

    private void setup() {
        JPanel panel = new JPanel(new CardLayout());
        firstCardPanel = new JPanel(new GridLayout(0, 1));

        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        Label description = new Label("Liczba znalezionych linków to: " + results.size());
        showListButton = new JButton("Pokaż listę");
        showListButton.addActionListener(this);
        topPanel.add(description);
        topPanel.add(showListButton);
        firstCardPanel.add(topPanel);

        JButton showSourceButton = new JButton("Pokaż źródło");
        showSourceButton.addActionListener(this);
        listPanel.add(listScrollPane);
        listPanel.add(showSourceButton);
        resultList = new JList(results.toArray());
        resultList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        listScrollPane.setViewportView(resultList);

        panel.add(firstCardPanel, "Wyniki");
        setMainPanel(panel, true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(showListButton)) {
            if (!isListShowing) {
                showListButton.setText("Schowaj listę");
                firstCardPanel.add(listPanel);
            } else {
                showListButton.setText("Pokaż listę");
                firstCardPanel.remove(listPanel);
            }
            isListShowing = !isListShowing;
            redraw();
        } else {
            String selectedUrl = results.get(resultList.getSelectedIndex());
            if (selectedUrl != null) {
                new SourceDisplayFrame("Źródło strony: " + selectedUrl, selectedUrl);
            }
        }
    }
}