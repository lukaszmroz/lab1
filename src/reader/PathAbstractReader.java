package reader;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public abstract class PathAbstractReader implements AbstractReader {

    protected String path;

    public PathAbstractReader(String path) {
        this.path = path;
    }
}
