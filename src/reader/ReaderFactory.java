package reader;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class ReaderFactory {
    public static AbstractReader create(ReaderType type, String path) {
        if (path.isEmpty()) {
            return null;
        }
        switch (type) {
            case File:
                return new FileReader(path);
            case URL:
            default:
                return new WebpageReader(path);
        }
    }
}
