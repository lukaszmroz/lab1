package reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class FileReader extends PathAbstractReader {

    public FileReader(String path) {
        super(path);
    }

    public String read() {
        byte[] encoded = new byte[0];
        try {
            encoded = Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            System.out.println("Can not read file.");
            e.printStackTrace();
        }
        return new String(encoded);
    }
}
