package reader;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public enum ReaderType {
    File, URL
}
