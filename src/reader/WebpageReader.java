package reader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.*;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class WebpageReader extends PathAbstractReader {

    public WebpageReader(String path) {
        super(path);
    }

    public String read() {
        StringBuilder b = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(new URL(path).openStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                b.append(inputLine);
                b.append(System.getProperty("line.separator"));
            }
        } catch (Exception e) {
            System.out.println("Can not read URL");
            e.printStackTrace();
        }
        return b.toString();
    }
}
