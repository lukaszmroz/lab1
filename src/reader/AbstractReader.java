package reader;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public interface AbstractReader {
    String read();
}
