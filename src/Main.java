import java.awt.*;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class Main {

    public static void main(String[] args) {
        EventQueue.invokeLater(MainFrame::new);
    }
}
