import javax.swing.*;
import reader.ReaderType;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class PathGetter {

    public static String getPath(ReaderType type) {
        switch (type) {
            case File:
                return getPathBySelectingFile();
            default:
            case URL:
                return getPathByEnteringURLString();
        }
    }


    private static String getPathByEnteringURLString() {
        return JOptionPane.showInputDialog("Podaj adres do strony www:");
    }

    /**
     * Get file path by showing open file dialog.
     *
     * @return String
     */
    private static String getPathBySelectingFile() {
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile().getPath();
        } else {
            return "";
        }
    }
}
