import javax.swing.*;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public abstract class CenteredFrame extends JFrame {

    protected JPanel mainPanel;

    public CenteredFrame(String name) {
        super(name);
    }

    public void setMainPanel(JPanel panel, boolean pack) {
        mainPanel = panel;
        getContentPane().add(mainPanel);
        if (pack)
            pack(); // Resizes window to fit current layout
        setVisible(true);
        setLocationRelativeTo(null); // Centers the window
    }

    public void redraw() {
        mainPanel.revalidate();
        mainPanel.repaint();
        pack();
        setLocationRelativeTo(null); // Centers the window
    }
}
