import reader.AbstractReader;
import reader.ReaderFactory;
import reader.ReaderType;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Łukasz Mróz on 17.10.15.
 */
public class SourceDisplayFrame extends CenteredFrame {

    public SourceDisplayFrame(String name, String url) {
        super(name);
        setup(url);
    }

    private void setup(String url) {
        AbstractReader reader = ReaderFactory.create(ReaderType.URL, url);
        String source = reader.read();
        if (source != null) {
            JPanel panel = new JPanel(new BorderLayout());
            setSize(600, 600);
            JTextArea textPane = new JTextArea(source);
            JScrollPane scrollPane = new JScrollPane(textPane);
            panel.add(scrollPane, BorderLayout.CENTER);

            setMainPanel(panel, false);
        }
    }
}
